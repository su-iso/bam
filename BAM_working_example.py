from BAM import *

filter_line_1 = ["OS", "contains", "Win"]

filter_line_2 = ["OS", "does not contain", "XP", "and", "2003", "and", "2008", "and", "2012"]

filter_line_3 = ["EMET Service Version ([STANFORD] Microsoft Enhanced Mitigation Experience Toolkit Information (EMET))", "contains", "5.", "or", "4.", "or", "3."]

filter_line_4 = ["EMET Service Channel ([STANFORD] Microsoft Enhanced Mitigation Experience Toolkit Information (EMET))", "is", "alpha", "or", "beta", "or", "production"]

master_filter = [filter_line_1, filter_line_2, filter_line_3, filter_line_4]

returned_columns = ["EMET Auto_Backdown Simple Health Check", "OS"]

username = "fakeusername"
password = "fakepassword"
website = "https://bfreports.stanford.edu"

MyQuery = relevance_query(returned_columns, master_filter, "computers")

MyResults = soap_query(username, password, website, MyQuery.Query)

MyObject = create(MyQuery, MyResults)

print MyObject.Objects[0]

print MyObject.Objects[0][0]

print MyObject.Objects[0][0][0]

