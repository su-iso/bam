﻿from BAM import *
import getpass

f = open("LOGS", "w")

def num_percent(type, grouptype, name, username, password): 

    """num_percent gets the number of triggered/override files of a group or subgroup. Also gives what percent of this group/subgroup is triggered.
    Type refers to trigger or override. (string).
    Grouptype refers to SU group, SU subgroup, or All. (string).
    Name refers to the name of the SU group or SU Subgroup. If grouptype is All, None can be passed in. (string).
    Username refers to the username of the bigfix credentials.
    Password refers to the password of the bigfix credentials"""

    f.write(" <num_percent> : <takes in type, grouptype, name, username, and password to find % and # of for a certain group> \n")
    answer = []
    if (type == "override"):
        if (grouptype == "All" or grouptype == "all" or grouptype == "All"):
            answer = override_all(username, password)
        if (grouptype == "SU Group" or grouptype == "SU group"):
            answer = override_group(name, username, password)
        if (grouptype == "SU subgroup" or grouptype == "SU Subgroup" or grouptype == "SU SubGroup"):
            answer = override_subgroup(name, username, password)
    elif (type == "trigger"):
        if (grouptype.lower() == "all"):
            answer = trigger_all(username, password)
        if (grouptype == "SU Group" or grouptype == "SU group"):
            answer = trigger_group(name, username, password)
        if (grouptype == "SU Subgroup" or grouptype == "SU subgroup" or grouptype == "SU SubGroup"):
            answer = trigger_subgroup(name, username, password)
    f.write(" <num_percent> : <returning 'answer', containing percentage and number of occurences> \n")
    return answer

def override_all(username, password):

    """Gets the override files of the entire database and returns the total number and percent overridden."""

    f.write(" <override_all> : <username and password passed in to find % and # of overrides> \n")
    returns = ["EMET Configuration Override Files"]
    filters = ["EMET Configuration Override Files", "does not contain", "none", "EMET Configuration Override Files", "does not contain", "emet service not found"]
    search = "Computers"
    temp_query = relevance_query(returns, filters, search)
    temp_result = soap_query(username, password, "https://bfreports.stanford.edu/webreports", temp_query.Query)
    trigger_counter = len(temp_result)
    relevance = "number of results from (bes computers) whose (value of it is not \"-\") of bes property \"Last Report Time\""
    total_result = soap_query(username, password, "https://bfreports.stanford.edu/webreports", relevance)
    total_num = total_result[0]
    percent = (trigger_counter/float(total_num)) * 100
    ans = [trigger_counter, percent]
    f.write(" <override_all> : <returning answer> \n")
    return ans

def trigger_all(username, password):

    """Gets the override files of the entire database and returns the total number and percent overridden."""

    f.write(" <trigger_all> : <passing in username and password to find # and % of triggers> \n")
    returns = ["EMET Triggered Mitigations for 30 Days"]
    filters = ["EMET Triggered Mitigations for 30 Days", "does not contain", "none", "EMET Triggered Mitigations for 30 Days", "does not contain", "emet not installed"]
    search = "Computers"
    temp_query = relevance_query(returns, filters, search)
    temp_result = soap_query(username, password, "https://bfreports.stanford.edu/webreports", temp_query.Query)
    trigger_counter = len(temp_result)
    relevance = "number of results from (bes computers) whose (value of it is not \"-\") of bes property \"Last Report Time\""
    total_result = soap_query(username, password, "https://bfreports.stanford.edu/webreports", relevance)
    total_num = total_result[0]
    percent = (trigger_counter/float(total_num)) * 100
    ans = [trigger_counter, percent]
    f.write(" <trigger_all> : <returning answer> \n")
    return ans

def override_group(name, username, password):
    if (name.find("|") != -1):
        name = name[name.index("|")+1:]

    """Gets the number of values and percentages in a SU group that are returned for "EMET Configuration Override Files"""

    f.write(" <override_group> : <passing in name of group and credentials to find # and % of group overrides> \n")
    returns = ["EMET Configuration Override Files"]
    filters = ["SU Group", "starts with", name]
    search = "Computers"
    temp_query = relevance_query(returns, filters, search)
    temp_result = soap_query(username, password, "https://bfreports.stanford.edu/webreports", temp_query.Query)
    trigger_counter = 0

    #Counts the number of overrides in this group
    for x in range (0, len(temp_result)):
        if (temp_result[x].lower() != u"none" and temp_result[x].lower() != u"emet service not found"):
            trigger_counter = trigger_counter + 1

    relevance = "number of results from (bes computers) whose (value of it as lowercase is \"" +  name.lower() + "\") of bes property \"su group\""
    total_result = soap_query(username, password, "https://bfreports.stanford.edu/webreports", relevance)
    total_counter = total_result[0]
    percent = (trigger_counter/float(total_counter)) * 100
    ans = [trigger_counter, percent]
    f.write(" <override_group> : <returning answer> \n")
    return ans

def trigger_group(name, username, password):

    """Gets the number of values and percentages in a SU group that are returned for "EMET Triggered Mitigations for 30 days."""

    f.write(" <trigger_group> : <passing in name of group and credentials to find # and % of triggers> \n")
    if (name.find("|") != -1):
        name = name[name.index("|")+1:]    

    returns = ["EMET Triggered Mitigations for 30 days"]
    filters = ["SU Group", "starts with", name]
    search = "Computers"
    temp_query = relevance_query(returns, filters, search)
    temp_result = soap_query(username, password, "https://bfreports.stanford.edu/webreports", temp_query.Query)
    trigger_counter = 0

    #Counts the number of triggers in this group
    for x in range (0, len(temp_result)):
        if (temp_result[x].lower() != u"none" and temp_result[x].lower() != u"emet not installed"):
            trigger_counter = trigger_counter + 1

    relevance = "number of results from (bes computers) whose (value of it is \"" +  name + "\") of bes property \"su group\""
    total_result = soap_query(username, password, "https://bfreports.stanford.edu/webreports", relevance)
    total_counter = total_result[0]
    percent = (trigger_counter/float(total_counter)) * 100
    ans = [trigger_counter, percent]
    f.write(" <trigger_group> : <returning answer> \n")
    return ans

def override_subgroup(name, username, password):

    """Gets the number of values and percentages in a SU subgroup that are returned for "EMET Configuration Override Files"""
    
    f.write(" <override_subgroup> : <passing in name and credentials to finr # and % of overrides in subgroup> \n")
    if (name.find("|") != -1):
        name = name[name.index("|")+1:]

    name = name.lower()
    returns = ["EMET Configuration Override Files"]
    filters = ["SU subgroup", "starts with", name]
    search = "Computers"
    temp_query = relevance_query(returns, filters, search)
    temp_result = soap_query(username, password, "https://bfreports.stanford.edu/webreports", temp_query.Query)
    trigger_counter = 0

    #Counts the number of triggers in this group
    for x in range (0, len(temp_result)):
        if (temp_result[x].lower() != u"none" and temp_result[x].lower() != u"emet service not found"):
            trigger_counter = trigger_counter + 1

    relevance = "number of results from (bes computers) whose (value of it as lowercase is \"" +  name + "\") of bes property \"su subgroup\""
    total_result = soap_query(username, password, "https://bfreports.stanford.edu/webreports", relevance)
    total_counter = total_result[0]

    percent = (trigger_counter/float(total_counter)) * 100
    ans = [trigger_counter, percent]
    f.write(" <override_subgroup> : <returning answer> \n")

    return ans

def trigger_subgroup(name, username, password):

    """Gets the number of values and percentages in a SU subgroup that are returned for "EMET Triggered Mitigations for 30 days"""
    
    f.write("<trigger_subgroup> : <passing in name and credentials to find # and % of triggers in subgroup> \n")
    if (name.find("|") != -1):
        name = name[name.index("|")+1:]    

    returns = ["EMET Triggered Mitigations for 30 days"]
    filters = ["SU subroup", "starts with", name]
    search = "Computers"
    temp_query = relevance_query(returns, filters, search)
    temp_result = soap_query(username, password, "https://bfreports.stanford.edu/webreports", temp_query.Query)
    trigger_counter = 0

    #Counts the number of triggers in this group
    for x in range (0, len(temp_result)):
        if (temp_result[x].lower() != u"none" and temp_result[x].lower() != u"emet not installed"):
            trigger_counter = trigger_counter + 1

    relevance = "number of results from (bes computers) whose (value of it is \"" +  name + "\") of bes property \"su subgroup\""
    total_result = soap_query(username, password, "https://bfreports.stanford.edu/webreports", relevance)
    total_counter = total_result[0]
    percent = (trigger_counter/float(total_counter)) * 100
    ans = [trigger_counter, percent]
    f.write(" <trigger_subgroup> : <returning answer> \n")
    return ans

#ALERTS BY PERCENT

def percent_check(global_percent, group_percent, sub_group_percent, username, password):
    '''Percent check takes in percentages of global, group, and subgroups, as well as BigFix credentials.
    Prints out wether these were above or below set percentage, and allows for viewing alerts.'''

    f.write(" <percent_check> : <passing in global, group, and subgrop percentages and credentials to find % and #s> \n")
    #GATHER DATA FROM EMET 
    returns = ["Emet Configuration Override Files", "Emet Triggered Mitigations for 30 days", "Computer Name", "Su group", "Su subgroup"]
    #filters = ["Emet Configuration Override Files", "does not contain", "none", "Emet Configuration Override Files", "does not contain", "service not found"]
    filters = ["Emet Configuration Override Files", "does not contain", "none", "Emet Configuration Override Files", "does not contain", "service not found", "OR", "not reported"]
    search = "computers"
    temp_query = relevance_query(returns, filters, search)
    temp_result = soap_query(username, password, "https://bfreports.stanford.edu/webreports", temp_query.Query)
    temp_save = create(temp_query, temp_result)

    #Deletes unneccesary fields in Emet Triggered Mitigations
    for x in range(0, len(temp_save.Objects)):
        y = 1
        if (temp_save.Objects[x][y][0].find("TIME") != -1): 
            temp_save.Objects[x][y].pop(0)
            temp_save.Objects[x][y].pop(0)
            temp_save.Objects[x][y].pop(1)
        else:
            temp_save.Objects[x][y].pop(0)
            temp_save.Objects[x][y].pop(0)
    
    #Deletes duplicate values
    delete_list = []
    for x in range(0, len(temp_save.Objects)):
        if (x == len(temp_save.Objects)-1):
            continue
        elif (temp_save.Objects[x] == temp_save.Objects[x+1]):
            delete_list.append(x)
        else:
            continue
    for x in range(0, len(delete_list)):
        temp_save.Objects.pop(delete_list[x])
        for y in range(0, len(delete_list)):
            delete_list[y] = delete_list[y]-1

    #ADDS TAGS TO VARIABLES
    for x in range(0, len(temp_save.Objects)):
        temp_save.Objects[x][0][0] = "NAME|" + temp_save.Objects[x][0][0]
        temp_save.Objects[x][2][0] = "COMPUTER NAME|" + temp_save.Objects[x][2][0]
        temp_save.Objects[x][3][0] = "SU-GROUP|" + temp_save.Objects[x][3][0]
        temp_save.Objects[x][4][0] = "SU-SUBGROUP|" + temp_save.Objects[x][4][0]


    #CHECKS GLOBAL OVERRIDE VALUES AND PERCENTAGES
    #(Both in global value)
    print "GLOBAL \n"

    global_value = num_percent("override", "all", None, username, password)
    if (global_value[1] > global_percent):
        print "Global percent is over ", global_percent, " at ", global_value[1], " percent, and ", global_value[0] , "occurrences."
        ans = raw_input( "Would you like to see the alerts? ")
        if ( ans == "yes" ):
            temp_save.print_index()

    else:
        print "Global percentage under set percentage. \n  Set percentage = ", global_percent, "\n  Global percentage = ", global_value[1], 
    print "\n"

    #CHECKS SU GROUP VALUES AND PERCENTAGES
    print "SU GROUPS \n"

    #Get the range of SU Groups affected
    SUGroups = []
    for x in range(0, len(temp_save.Objects)):
        if (temp_save.Objects[x][3] not in SUGroups):
            SUGroups.append(temp_save.Objects[x][3])
        else:
            continue

    #Get the number and percents of SU Groups 
    group_names = []
    group_num_percents = []
    for x in range(0, len(SUGroups)):
        group_num_percents.append(num_percent("override", "SU Group", SUGroups[x][0], username, password))
        group_names.append(SUGroups[x][0][SUGroups[x][0].index("|")+1:])
  
    for x in range(0, len(group_num_percents)):
        if (group_num_percents[x][1] > group_percent):
            print group_names[x], "Group's percent is over", group_percent, " percent, at ", group_num_percents[x][1], " perecnt, and ", group_num_percents[x][0], " occurences.\n"
            ans = raw_input("Would you like to see the alerts? ")
            print "\n"
            if (ans == "yes"):
                for y in range (0, len(temp_save.Objects)):
                    if (temp_save.Objects[y][3][0][temp_save.Objects[y][3][0].index("|")+1:] == group_names[x]):
                        for z in range(0, len(temp_save.Objects[y])):
                            for a in range(0, len(temp_save.Objects[y][z])):
                                print temp_save.Objects[y][z][a]
                        print "\n"
                print "\n"
        elif (x == len(group_num_percents)-1):
            print "No SU Groups over set percentage. \n  Set percentage = ", group_percent, " \n  Subgroup percentages: "
            for x in range(0, len(group_num_percents)):
                print "  ", group_names[x], "at", group_num_percents[x][1], "percent."
            print "\n"


    #CHECKS SUBGROUP VALUES AND PERCENTAGES
    print "SU SUBGROUPS \n"

    #Get the range of SU Subgroups affected
    SUSubgroups = []
    for x in range(0, len(temp_save.Objects)):
        if (temp_save.Objects[x][4] not in SUSubgroups and temp_save.Objects[x][4][0][temp_save.Objects[x][4][0].index("|")+1:] != u'not set'):
            SUSubgroups.append(temp_save.Objects[x][4])
        else:
            continue

    #Get the number and percents of SU Subgroups 
    subgroup_names = []
    subgroup_num_percents = []
    for x in range(0, len(SUSubgroups)):
        subgroup_num_percents.append(num_percent("override", "SU Subgroup", SUSubgroups[x][0], username, password))
        subgroup_names.append(SUSubgroups[x][0][SUSubgroups[x][0].index("|")+1:])
 
    for x in range(0, len(subgroup_num_percents)):
        if (subgroup_num_percents[x][1] > sub_group_percent):
            print subgroup_names[x], "Subgroup's percent is over ", sub_group_percent, " percent, at ", subgroup_num_percents[x][1], " perecnt, and ", subgroup_num_percents[x][0], " occurences. \n"
            ans = raw_input("Would you like to see the alerts? ")
            print "\n"
            if (ans == "yes"):
                for y in range (0, len(temp_save.Objects)):
                    if (temp_save.Objects[y][4][0][temp_save.Objects[y][4][0].index("|")+1:] == subgroup_names[x]):
                        for z in range(0, len(temp_save.Objects[y])):
                            for a in range(0, len(temp_save.Objects[y][z])):
                                print temp_save.Objects[y][z][a]
                        print "\n"
        elif (x == len(group_num_percents)-1):
            print "No SU Subgroups over set percentage. \n  Set percentage = ", sub_group_percent, " \n  Subgroup percentages: "
            for x in range(0, len(subgroup_num_percents)):
                print "  ", subgroup_names[x], "at", subgroup_num_percents[x][1], "percent, and", subgroup_num_percents[x][0], "occurrences."
            print "\n"

    f.write(" <percent_check> : <exiting> \n")


#ALERTS BY # OCCURENCE
def num_check(global_num, group_num, sub_group_num, username, password):
     
    '''num_check allows the global, group, and subgroup numbers to be analyzed.
     A number for each must be passed in, as well as the user's BigFix credentials. 
     If the number of occurences are surpassed, the user will have the option to view alerts.'''

    #GATHER DATA FROM EMET 

    returns = ["Emet Configuration Override Files", "Emet Triggered Mitigations for 30 days", "Computer Name", "Su group", "Su subgroup"]
    
    #filters = ["Emet Configuration Override Files", "does not contain", "none", "Emet Configuration Override Files", "does not contain", "service not found"]
    
    filters = ["Emet Configuration Override Files", "does not contain", "none", "Emet Configuration Override Files", "does not contain", "service not found", "AND", "not reported"]
    search = "computers"
    temp_query = relevance_query(returns, filters, search)
    temp_result = soap_query(username, password, "https://bfreports.stanford.edu/webreports", temp_query.Query)
    temp_save = create(temp_query, temp_result)

    #Deletes unneccesary fields in Emet Triggered Mitigations
    for x in range(0, len(temp_save.Objects)):
        y = 1
        if (temp_save.Objects[x][y][0].find("TIME") != -1): 
            temp_save.Objects[x][y].pop(0)
            temp_save.Objects[x][y].pop(0)
            temp_save.Objects[x][y].pop(1)
        else:
            temp_save.Objects[x][y].pop(0)
            temp_save.Objects[x][y].pop(0)
    
    #Deletes duplicate values
    delete_list = []
    for x in range(0, len(temp_save.Objects)):
        if (x == len(temp_save.Objects)-1):
            continue
        elif (temp_save.Objects[x] == temp_save.Objects[x+1]):
            delete_list.append(x)
        else:
            continue
    for x in range(0, len(delete_list)):
        temp_save.Objects.pop(delete_list[x])
        for y in range(0, len(delete_list)):
            delete_list[y] = delete_list[y]-1

    #ADDS TAGS TO VARIABLES
    for x in range(0, len(temp_save.Objects)):
        temp_save.Objects[x][0][0] = "NAME|" + temp_save.Objects[x][0][0]
        temp_save.Objects[x][2][0] = "COMPUTER NAME|" + temp_save.Objects[x][2][0]
        temp_save.Objects[x][3][0] = "SU-GROUP|" + temp_save.Objects[x][3][0]
        temp_save.Objects[x][4][0] = "SU-SUBGROUP|" + temp_save.Objects[x][4][0]


    #cHECKS GLOBAL OVERRIDE VALUES AND PERCENTAGES
    #(Both in global value)
    print "GLOBAL \n"

    global_value = num_percent("override", "all", None, username, password)
    if (global_value[0] > global_num):
        print "Global occurences are over", global_num, " at", global_value[0] , ". \n"
        ans = raw_input( "Would you like to see the alerts? ")
        if ( ans == "yes" ):
            temp_save.print_index()

    else:
        print "Global occurence under set occurence. \n  Set occurence = ", global_num, "\n  Global occurences = ", global_value[0], 
    print "\n"

    #CHECKS SU GROUP VALUES AND PERCENTAGES
    print "SU GROUPS \n"

    #Get the range of SU Groups affected
    SUGroups = []
    for x in range(0, len(temp_save.Objects)):
        if (temp_save.Objects[x][3] not in SUGroups):
            SUGroups.append(temp_save.Objects[x][3])
        else:
            continue

    #Get the number and percents of SU Groups 
    group_names = []
    group_num_percents = []
    for x in range(0, len(SUGroups)):
        group_num_percents.append(num_percent("override", "SU Group", SUGroups[x][0], username, password))
        group_names.append(SUGroups[x][0][SUGroups[x][0].index("|")+1:])
  
    for x in range(0, len(group_num_percents)):
        if (group_num_percents[x][0] > group_num):
            print group_names[x], "Group's occurences are over", group_num, " at", group_num_percents[x][0], ".\n"
            ans = raw_input("Would you like to see the alerts? ")
            print "\n"
            if (ans == "yes"):
                for y in range (0, len(temp_save.Objects)):
                    if (temp_save.Objects[y][3][0][temp_save.Objects[y][3][0].index("|")+1:] == group_names[x]):
                        for z in range(0, len(temp_save.Objects[y])):
                            for a in range(0, len(temp_save.Objects[y][z])):
                                print temp_save.Objects[y][z][a]
                        print "\n"
                print "\n"
        elif (x == len(group_num_percents)-1):
            print "No SU Groups over set occurence number. \n  Set occurence = ", group_percent, " \n  Subgroup occurence: "
            for x in range(0, len(group_num_percents)):
                print "  ", group_names[x], "at", group_num_percents[x][0], "occurrences. \n"
            print "\n"


    #CHECKS SUBGROUP VALUES AND PERCENTAGES
    print "SU SUBGROUPS \n"

    #Get the range of SU Subgroups affected
    SUSubgroups = []
    for x in range(0, len(temp_save.Objects)):
        if (temp_save.Objects[x][4] not in SUSubgroups and temp_save.Objects[x][4][0][temp_save.Objects[x][4][0].index("|")+1:] != u'not set'):
            SUSubgroups.append(temp_save.Objects[x][4])
        else:
            continue

    #Get the number and percents of SU Subgroups 
    subgroup_names = []
    subgroup_num_percents = []
    for x in range(0, len(SUSubgroups)):
        subgroup_num_percents.append(num_percent("override", "SU Subgroup", SUSubgroups[x][0], username, password))
        subgroup_names.append(SUSubgroups[x][0][SUSubgroups[x][0].index("|")+1:])
 
    for x in range(0, len(subgroup_num_percents)):
        if (subgroup_num_percents[x][0] > sub_group_num):
            print subgroup_names[x], "Subgroup's number of occurences is over ", sub_group_num, "occurences at", subgroup_num_percents[x][0], ". \n"
            ans = raw_input("Would you like to see the alerts? ")
            print "\n"
            if (ans == "yes"):
                for y in range (0, len(temp_save.Objects)):
                    if (temp_save.Objects[y][4][0][temp_save.Objects[y][4][0].index("|")+1:] == subgroup_names[x]):
                        for z in range(0, len(temp_save.Objects[y])):
                            for a in range(0, len(temp_save.Objects[y][z])):
                                print temp_save.Objects[y][z][a]
                        print "\n"
        elif (x == len(group_num_percents)-1):
            print "No SU Subgroups over occurence. \n  Set occurence = ", sub_group_num, " \n  Subgroup occurences: "
            for x in range(0, len(subgroup_num_percents)):
                print "  ", subgroup_names[x], "at", subgroup_num_percents[x][0], "occurrences."
            print "\n"
    f.write(" <all_check> : <exiting> \n")


#CALL TO TRIGGER ALERTS
def all_check(global_percent, global_num, group_percent, group_num, sub_group_percent, sub_group_num, username, password):
     
    '''all_check allows the global, group, and subgroup percentages and numbers to be analyzed.
     A number for each must be passed in, as well as the user's BigFix credentials. 
     If the percent and number of occurences are surpassed, the user will have the option to view alerts.'''

    #GATHER DATA FROM EMET 

    returns = ["Emet Configuration Override Files", "Emet Triggered Mitigations for 30 days", "Computer Name", "Su group", "Su subgroup"]
    
    #filters = ["Emet Configuration Override Files", "does not contain", "none", "Emet Configuration Override Files", "does not contain", "service not found"]
    
    filters = ["Emet Configuration Override Files", "does not contain", "none", "Emet Configuration Override Files", "does not contain", "service not found", "AND", "not reported"]
    search = "computers"
    temp_query = relevance_query(returns, filters, search)
    temp_result = soap_query(username, password, "https://bfreports.stanford.edu/webreports", temp_query.Query)
    temp_save = create(temp_query, temp_result)

    #Deletes unneccesary fields in Emet Triggered Mitigations
    for x in range(0, len(temp_save.Objects)):
        y = 1
        if (temp_save.Objects[x][y][0].find("TIME") != -1): 
            temp_save.Objects[x][y].pop(0)
            temp_save.Objects[x][y].pop(0)
            temp_save.Objects[x][y].pop(1)
        else:
            temp_save.Objects[x][y].pop(0)
            temp_save.Objects[x][y].pop(0)
    
    #Deletes duplicate values
    delete_list = []
    for x in range(0, len(temp_save.Objects)):
        if (x == len(temp_save.Objects)-1):
            continue
        elif (temp_save.Objects[x] == temp_save.Objects[x+1]):
            delete_list.append(x)
        else:
            continue
    for x in range(0, len(delete_list)):
        temp_save.Objects.pop(delete_list[x])
        for y in range(0, len(delete_list)):
            delete_list[y] = delete_list[y]-1

    #ADDS TAGS TO VARIABLES
    for x in range(0, len(temp_save.Objects)):
        temp_save.Objects[x][0][0] = "NAME|" + temp_save.Objects[x][0][0]
        temp_save.Objects[x][2][0] = "COMPUTER NAME|" + temp_save.Objects[x][2][0]
        temp_save.Objects[x][3][0] = "SU-GROUP|" + temp_save.Objects[x][3][0]
        temp_save.Objects[x][4][0] = "SU-SUBGROUP|" + temp_save.Objects[x][4][0]


    #cHECKS GLOBAL OVERRIDE VALUES AND PERCENTAGES
    #(Both in global value)
    print "GLOBAL \n"

    global_value = num_percent("override", "all", None, username, password)
    if (global_value[1] > global_percent and global_value[0] > global_num):
        print "Global percent is over", global_percent, "at", global_value[1], ", and over", global_num, "occurences at", global_value[0] , ". \n"
        ans = raw_input( "Would you like to see the alerts? ")
        if ( ans == "yes" ):
            temp_save.print_index()

    elif (global_value[0] < global_num):
        print "Global percentage under set percentage. \n  Set percentage = ", global_percent, "\n  Global percentage = ", global_value[1], 
    else:
        print "Global occurences under set occurences. \n  Set occurences = ", global_num, "\n Global percentage = ", global_value[0]
    print "\n"

    #CHECKS SU GROUP VALUES AND PERCENTAGES
    print "SU GROUPS \n"

    #Get the range of SU Groups affected
    SUGroups = []
    for x in range(0, len(temp_save.Objects)):
        if (temp_save.Objects[x][3] not in SUGroups):
            SUGroups.append(temp_save.Objects[x][3])
        else:
            continue

    #Get the number and percents of SU Groups 
    group_names = []
    group_num_percents = []
    for x in range(0, len(SUGroups)):
        group_num_percents.append(num_percent("override", "SU Group", SUGroups[x][0], username, password))
        group_names.append(SUGroups[x][0][SUGroups[x][0].index("|")+1:])
  
    for x in range(0, len(group_num_percents)):
        if (group_num_percents[x][1] > group_percent and group_num_percents[x][0] > group_num):
            print group_names[x], "Group's percent is over", group_percent, "percent, at", group_num_percents[x][1], ", and over", group_num, "occurences at", group_num_percents[x][0], ".\n"
            ans = raw_input("Would you like to see the alerts? ")
            print "\n"
            if (ans == "yes"):
                for y in range (0, len(temp_save.Objects)):
                    if (temp_save.Objects[y][3][0][temp_save.Objects[y][3][0].index("|")+1:] == group_names[x]):
                        for z in range(0, len(temp_save.Objects[y])):
                            for a in range(0, len(temp_save.Objects[y][z])):
                                print temp_save.Objects[y][z][a]
                        print "\n"
                print "\n"
        elif (x == len(group_num_percents)-1):
            print "No SU Groups over set percentage and occurence number. \n  Set percentage = ", group_percent, "Set occurence number = ", group_num, " \n  Groups: "
            for x in range(0, len(group_num_percents)):
                print "  ", group_names[x], "at", group_num_percents[x][1], "percent, and", group_num_percents[x][0], "occurrences. \n"
            print "\n"


    #CHECKS SUBGROUP VALUES AND PERCENTAGES
    print "SU SUBGROUPS \n"

    #Get the range of SU Subgroups affected
    SUSubgroups = []
    for x in range(0, len(temp_save.Objects)):
        if (temp_save.Objects[x][4] not in SUSubgroups and temp_save.Objects[x][4][0][temp_save.Objects[x][4][0].index("|")+1:] != u'not set'):
            SUSubgroups.append(temp_save.Objects[x][4])
        else:
            continue

    #Get the number and percents of SU Subgroups 
    subgroup_names = []
    subgroup_num_percents = []
    for x in range(0, len(SUSubgroups)):
        subgroup_num_percents.append(num_percent("override", "SU Subgroup", SUSubgroups[x][0], username, password))
        subgroup_names.append(SUSubgroups[x][0][SUSubgroups[x][0].index("|")+1:])
 
    for x in range(0, len(subgroup_num_percents)):
        if (subgroup_num_percents[x][1] > sub_group_percent and subgroup_num_percents[x][0] > sub_group_num):
            print subgroup_names[x], "Subgroup's percent is over", sub_group_percent, "percent, at", subgroup_num_percents[x][1], ", and over", sub_group_num, "occurences at", subgroup_num_percents[x][0], ". \n"
            ans = raw_input("Would you like to see the alerts? ")
            print "\n"
            if (ans == "yes"):
                for y in range (0, len(temp_save.Objects)):
                    if (temp_save.Objects[y][4][0][temp_save.Objects[y][4][0].index("|")+1:] == subgroup_names[x]):
                        for z in range(0, len(temp_save.Objects[y])):
                            for a in range(0, len(temp_save.Objects[y][z])):
                                print temp_save.Objects[y][z][a]
                        print "\n"
        elif (x == len(group_num_percents)-1):
            print "No SU Subgroups over set percentage. \n  Set percentage = ", "Set occurence = ", sub_group_num, sub_group_percent, " \n  Subgroups: "
            for x in range(0, len(subgroup_num_percents)): 
                print "  ", subgroup_names[x], "at", subgroup_num_percents[x][1], "percent, and", subgroup_num_percents[x][0], "occurrences."
            print "\n"

    f.write(" <all_check> : <exiting> \n")

