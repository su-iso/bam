from BAM import *

#Input your username and password
#Creates a file which contains events to be uploaded into splunk.
def create_events(username, password):

    #Decides what events will get pulled from BF
    filters = ["computer name", "starts with", "a"]

    #Pulling properties for Events
    returns = ["computer name", "su subgroup", "su group", "ip address", "os", "emet triggered mitigations for 30 days", ]
    search = "computers"

    #Creating relevance query and fetching results (INPUT YOUR USERNAME/PASSWORD)
    query = relevance_query(returns, filters, search)
    results = soap_query(username, password, "https://bfreports.stanford.edu", query.Query)


    seen = []
    for each in range(0, len(results)): #Deletes duplicates
        if (results[each] not in seen):
            seen.append(results[each])

    print seen

    save = create(query, results) #structure the results


    for each in range(0, len(results)): #Removes keys in Emet Mitigation property

        if (len(save.Objects[each][5]) > 1): 
            if (save.Objects[each][5][0].find("|")):
                save.Objects[each][5][0] = save.Objects[each][5][0][5:]

            if (save.Objects[each][5][1].find("|")):
                save.Objects[each][5][1] = save.Objects[each][5][1][11:]

            if (save.Objects[each][5][2].find("|")):
                save.Objects[each][5][2] = save.Objects[each][5][2][4:]

            if (save.Objects[each][5][3].find("|")):
                save.Objects[each][5][3] = save.Objects[each][5][3][12:]

            if (save.Objects[each][5][4].find("|")):
                save.Objects[each][5][4] = save.Objects[each][5][4][8:]

            if (save.Objects[each][5][5].find("|")):
                save.Objects[each][5][5] = save.Objects[each][5][5][5:]

            if (save.Objects[each][5][6].find("|")):
                save.Objects[each][5][6] = save.Objects[each][5][6][7:]

            if (save.Objects[each][5][7].find("|")):
                save.Objects[each][5][7] = save.Objects[each][5][7][4:]

            if (save.Objects[each][5][8].find("|")):
                save.Objects[each][5][8] = save.Objects[each][5][8][4:]

            if (save.Objects[each][5][9].find("|")):
                save.Objects[each][5][9] = save.Objects[each][5][9][11:]

            if (save.Objects[each][5][10].find("|")):
                save.Objects[each][5][10] = save.Objects[each][5][10][15:]

            if (save.Objects[each][5][11].find("|")):
                save.Objects[each][5][11] = save.Objects[each][5][11][16:]

    for each in range (0, len(save.Objects)): #Moves TIME property to front, deleltes unncecessary comma and day of the week
        time = save.Objects[each][5].pop(0)
        save.Objects[each].insert(0,time)
        save.Objects[each][0] = save.Objects[each][0][5:]

    filename = "splunk-events" #Name of file exported

    save.file_index(filename) #Writes out data into a file 

    #The file can be found in the folder "SplunkEventCreator.py" is found in.



create_events("username", "password")